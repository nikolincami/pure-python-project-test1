# Exercise 1 - Function that checks if a number is even
def check_even(num):
    if num % 2 == 0:
        return True  # Returns True if the given number is Even
    else:
        return False  # Returns False if the given number is Odd  $   #ks #


# Exercise 2 - Function that checks if a number is palindrome or not
def palindrome_num(num):
    if str(num) == str(num)[::-1]:  # Converts number to string and checks if its equal to the reversed string
        return "Is Palindrome"  # Returns "Is Palindrome" if the condition above is True
    else:
        return "It's not"  # Returns "It's not" if the condition is False


# Exercise 3 - Function that checks if a number is power of 2
def check_power(num):
    if num == 0:
        return "Can't divide by 0"  # Returns this message if the given param is 0
    while (num != 1):
        if (num % 2 != 0):  # If the remainder of num % 2 is different from 0 while num is different from 1
            return "Not power of 2"  # Returns Not power of 2
        num = num // 2  # Continues to divide by 2 until num = 1
    return "Power of 2"  # If num = 1 after division returns this message


# Exercise 4 - Function that returns a string in all uppercase if a certain condition is true
def string_uppercase(string):
    upper_letters = 0  # Initial upper letters set to 0
    for letter in string[0:4]:  # for loop to check letters from index 0 to 4
        if letter.upper() in letter:  # if it finds upper letters in the string up to index 4
            upper_letters += 1  # upper letters count increases with 1
    while upper_letters < 2:  # while having less than two uppercase letters
        return string  # it returns the original string
    else:
        return string.upper()  # else, it returns the string in all uppercase


# Exercise 5 - Function that iterates a list and returns numbers divisible by 5
def divisible_by_five():
    list1 = [12, 15, 32, 42, 55, 75, 122, 132, 150, 180, 200]
    for num in list1:  # Looping through the given list
        if num % 5 == 0 and num <= 150:  # if this condition is true, numbers that meet this conditions will be printed
            print(num)


# Exercise 6 - Function that creates a dictionary with word's length as keys and words themselves as values
def words_length_group(words):
    list1 = ['Lorem', 'ele', 'sit', 'incididunt', 'a', 'su', 'dom', 'ouch', 'ipsum']
    dict_output = {}  # Empty dict to add keys and values
    for word in list1:  # Looping through words in the given list
        words = len(word)  # Creating variable 'words' with word's length
        if words in dict_output:
            dict_output[words].add(word)  # Adding length as key
        else:
            dict_output[words] = {word}  # Adding word as value
    print(sorted(dict_output.items()))

# list1 = ['Lorem', 'ele', 'sit', 'incididunt', 'a', 'su', 'dom', 'ouch', 'ipsum']
# words_length_group(list1)


# Exercise 7 - Person as a parent class and Student as a child class
class Person:
    def __init__(self, first_name, second_name):
        self.name = first_name
        self.surname = second_name

    def get_fullname(self):
        return self.name + ' ' + self.surname

class Student(Person):
    def get_fullname(self):
        return self.name + ' ' + self.surname + " -st"

# person = Person("Nikolin", "Cami")
# print(Person.get_fullname(person))
# print(Student.get_fullname(person))
